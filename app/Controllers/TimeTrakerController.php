<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\CompanyModel;
use App\Models\ProjectModel;
use App\Models\UserModel;
use App\Models\TimeTrackerModel;

class TimeTrakerController extends BaseController
{
    public $session;
    public $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
        $this->session = \Config\Services::session();
    }

    // show users list
    public function index(){
        
        $sql="SELECT t.id, t.name, t.description, t.startdate, t.enddate, t.status, c.name AS company_name, p.name AS project_name, u.name AS user_name FROM `time_tracker` AS t, company c, project p, users u WHERE t.company_id=c.id and t.project_id=p.id and u.id=t.user_id;";
        $query = $this->db->query($sql);
        $data['tickets'] = $query->getResult();

        $timetracker=new TimeTrackerModel();
        $projectModel=new ProjectModel();
        $userModel=new UserModel();
        
        //$data['tickets']=$timetracker->findAll();
        $data['projects']=$projectModel->findAll();
        $data['users']=$userModel->findAll();
        
        return view('admin/time_tracker/view', $data);
    }
    // add user form
    public function create(){

        $data=[];
        $companyModel = new CompanyModel();
        $timetracker=new TimeTrackerModel();
        $projectModel=new ProjectModel();
        $userModel=new UserModel();
        
        $data['tickets']=$timetracker->findAll();
        $data['projects']=$projectModel->findAll();
        $data['users']=$userModel->findAll();

        return view('admin/time_tracker/create',$data);
    }
 
    // insert data
    public function store() {
        $errors = [];
        
        $ticketModel = new TimeTrackerModel();
        
        $projectModel = new ProjectModel();
        
        $rules = [
            'name' => 'required',
            'description' => 'required',
            'user' => 'required',
            'project' => 'required',
            'sdate' => 'required',
            'edate' => 'required',
        ];


        if (!$this->validate($rules, $errors)) {
            $this->session->setFlashdata("validation", $this->validator);
            return $this->response->redirect(site_url('/admin/time_tracker_create/'));
        } else {
            $project=$projectModel->where("id",$this->request->getVar('project'))->first();
            
            $ticketData = [
                'name' =>  $this->request->getVar('name'), 
                'description' =>  $this->request->getVar('description'),
                'company_id' =>  $project['company_id'],
                'user_id' =>  $this->request->getVar('user'),
                'project_id' =>  $this->request->getVar('project'),
                'startdate' =>  $this->request->getVar('sdate'),
                'enddate' =>  $this->request->getVar('edate'),
            ];
            $ticketModel->insert($ticketData);
        }
        $this->session->setFlashdata("success", "Tiecket Created Sucessfully");
        return $this->response->redirect(site_url('/admin/time_tracker/'));
    }

    // show single user
    public function edit($id = null){
        
        $data=[];
        $timetracker=new TimeTrackerModel();
        $projectModel=new ProjectModel();
        $userModel=new UserModel();
        
        $data['ticket']=$timetracker->where('id',$id)->first();
        $data['projects']=$projectModel->findAll();
        $data['users']=$userModel->findAll();
        
        return view('admin/time_tracker/edit', $data);
    }
    // update user data
    public function update(){
        
        $errors = [];
        
        $ticketModel = new TimeTrackerModel();
        
        $projectModel = new ProjectModel();
        
        $rules = [
            'name' => 'required',
            'description' => 'required',
            'user' => 'required',
            'project' => 'required',
            'sdate' => 'required',
            'edate' => 'required',
            'status' => 'required',
        ];


        if (!$this->validate($rules, $errors)) {
            $this->session->setFlashdata("validation", $this->validator);
            return $this->response->redirect(site_url('/admin/time_tracker_edit/'));
        } else {
            $project=$projectModel->where("id",$this->request->getVar('project'))->first();
            
            $ticketData = [
                'name' =>  $this->request->getVar('name'), 
                'description' =>  $this->request->getVar('description'),
                'company_id' =>  $project['company_id'],
                'user_id' =>  $this->request->getVar('user'),
                'project_id' =>  $this->request->getVar('project'),
                'startdate' =>  $this->request->getVar('sdate'),
                'enddate' =>  $this->request->getVar('edate'),
                'status' =>  $this->request->getVar('status'),
            ];
            $ticketModel->update($this->request->getVar('id'),$ticketData);
        }
        $this->session->setFlashdata("success", "Tiecket updated sucessfully");
        return $this->response->redirect(site_url('/admin/time_tracker/'));

    }
 
    //Delete ticket
    public function delete($id = null){
        $ticketModel = new TimeTrackerModel();
        $data['user'] = $ticketModel->where('id', $id)->delete($id);
        $this->session->setFlashdata("success", "Ticket Deleted Sucessfully");
        return $this->response->redirect(site_url('/admin/time_tracker/'));
    }  
}
