<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ProjectModel;
use App\Models\TimeTrackerModel;
use App\Models\CompanyModel;

class EmployeeController extends BaseController
{
    public function __construct()
    {
        if (session()->get('role') != "employee") {
            echo 'Access denied';
            exit;
        }
    }
    public function index()
    {
        $data=[];
        $projectModel=new ProjectModel();
        $ticketModel=new TimeTrackerModel();
        $companyModel=new CompanyModel();
        

        
        $data['company']=$companyModel->countAll();
        $data['project']=$projectModel->where('user_id',session()->get('id'))->countAll();
        $data['open_ticket']=$ticketModel->where('user_id',session()->get('id'))->where("status","created")->countAll();
        $data['close_ticket']=$ticketModel->where('user_id',session()->get('id'))->where("status","done")->countAll();
        $data['on_progress_ticket']=$ticketModel->where('user_id',session()->get('id'))->where("status","in_progress")->countAll();

        return view("employee/dashboard",$data);
    }
}
