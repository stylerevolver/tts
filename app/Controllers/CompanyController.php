<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\CompanyModel;
use App\Models\CityModel;


class CompanyController extends BaseController
{
    public $session;

    public $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
        $this->session = \Config\Services::session();
    }

    // show users list
    public function index(){
        
        
        $companymodel = new CompanyModel();
        $db = \Config\Database::connect();
        $sql="SELECT company.id,company.name,company.email,company.phoneno,company.address,city.name as city_name from company, city WHERE company.city_id=city.id";    
        $query = $db->query($sql);
        $data['companys'] = $query->getResult();
        return view('admin/company/view', $data);
    }
    // add user form
    public function create(){
        $data=[];
        $citymodel = new CityModel();
        $data['citys'] = $citymodel->findAll();
        return view('admin/company/create',$data);
    }
 
    // insert data
    public function store() {
        $errors = [];
        $model = new CompanyModel();
        
        $citymodel = new CityModel();
        $data['citys'] = $citymodel->findAll();
        
        $rules = [
            'name' => 'required',
            'email' => 'required|min_length[6]|max_length[50]|valid_email',
            'phoneno' => 'required',
            'address' => 'required|max_length[255]',
            'city' => 'required',
        ];

        

        if (!$this->validate($rules, $errors)) {
             $this->session->setFlashdata("validation", $this->validator);
            return $this->response->redirect(site_url('/admin/company_create/'));
        } else {
            $companyData = [
                'name' =>  $this->request->getVar('name'), 
                'email' =>  $this->request->getVar('email'),
                'phoneno' =>  $this->request->getVar('phoneno'),
                'address' =>  $this->request->getVar('address'),
                'city_id' =>  $this->request->getVar('city'),
            ];
            $model->insert($companyData);
        }
        $this->session->setFlashdata("success", "Company Created Sucessfully");
        return $this->response->redirect(site_url('/admin/company/'));
    }

    // show single user
    public function singleCompany($id = null){
        $moel = new CompanyModel();
        $citymodel = new CityModel();
        $data['citys'] = $citymodel->findAll();
        $data['company'] = $moel->where('id', $id)->first();
        
        return view('admin/company/edit', $data);
    }
    // update user data
    public function update(){
        $errors = [];
        $model = new CompanyModel();
        $citymodel = new CityModel();
        
        $rules = [
            'name' => 'required',
            'email' => 'required|min_length[6]|max_length[50]|valid_email',
            'phoneno' => 'required',
            'address' => 'required|max_length[255]',
            'city' => 'required',
        ];

        if (!$this->validate($rules, $errors)) {
            $this->session->setFlashdata("validation", $this->validator);
            return $this->response->redirect(site_url('/admin/company_edit/'.$this->request->getVar('id')));
        } else {
            $companyData = [
                'name' =>  $this->request->getVar('name'), 
                'email' =>  $this->request->getVar('email'),
                'phoneno' =>  $this->request->getVar('phoneno'),
                'address' =>  $this->request->getVar('address'),
                'city_id' =>  $this->request->getVar('city'),
            ];
            $id=$this->request->getVar('id');
            $model->update($id, $companyData);
        }
        $this->session->setFlashdata("success", "Company Updated Sucessfully");
        return $this->response->redirect(site_url('/admin/company/'));
    }
 
    // delete user
    public function delete($id = null){
        $userModel = new CompanyModel();
        $data['user'] = $userModel->where('id', $id)->delete($id);
        $this->session->setFlashdata("success", "Company Deleted Sucessfully");
        return $this->response->redirect(site_url('/admin/company/'));
    }  
}
