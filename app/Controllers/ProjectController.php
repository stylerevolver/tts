<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\CompanyModel;
use App\Models\ProjectModel;


class ProjectController extends BaseController
{
    public $session;
    public $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
        $this->session = \Config\Services::session();
    }

    // show users list
    public function index(){
        
        $sql="SELECT project.id, project.name, project.description, project.status, company.name AS company_name FROM project as project, company as company WHERE company.id = project.company_id;";    
        $query = $this->db->query($sql);
        $data['projects'] = $query->getResult();
        // print_r($data['projects']);exit();
        return view('admin/project/view', $data);
    }
    // add user form
    public function create(){

        $data=[];
        $companyModel = new CompanyModel();
        $data['company'] = $companyModel->findAll();
        return view('admin/project/create',$data);
    }
 
    // insert data
    public function store() {
        $errors = [];
        
        $model = new ProjectModel();
        
        $companyModel = new CompanyModel();
        $data['company'] = $companyModel->findAll();
        
        $rules = [
            'name' => 'required',
            'description' => 'required',
            'company' => 'required',
        ];

        
        if (!$this->validate($rules, $errors)) {
            $this->session->setFlashdata("validation", $this->validator);
            return $this->response->redirect(site_url('/admin/project_create/'));
        } else {
            $projectData = [
                'name' =>  $this->request->getVar('name'), 
                'description' =>  $this->request->getVar('description'),
                'company_id' =>  $this->request->getVar('company'),
            ];
            $model->insert($projectData);
        }
        $this->session->setFlashdata("success", "Project Created Sucessfully");
        return $this->response->redirect(site_url('/admin/project/'));
    }

    // show single user
    public function singleProject($id = null){
        $moel = new ProjectModel();
        $companyModel = new CompanyModel();
        $data['company'] = $companyModel->findAll();
        $data['project'] = $moel->where('id', $id)->first();
        
        return view('admin/project/edit', $data);
    }
    // update user data
    public function update(){
        $errors = [];
        
        $model = new ProjectModel();
        
        $companyModel = new CompanyModel();
        $data['company'] = $companyModel->findAll();
        
        $rules = [
            'name' => 'required',
            'description' => 'required',
            'company' => 'required',
        ];

        
        if (!$this->validate($rules, $errors)) {
            $this->session->setFlashdata("validation", $this->validator);
            return $this->response->redirect(site_url('/admin/project_edit/'.$this->request->getVar('id')));
        } else {
            $projectData = [
                'name' =>  $this->request->getVar('name'), 
                'description' =>  $this->request->getVar('description'),
                'company_id' =>  $this->request->getVar('company'),
            ];
            $model->update($this->request->getVar('id'),$projectData);
        }
        $this->session->setFlashdata("success", "Project Updated Sucessfully");
        return $this->response->redirect(site_url('/admin/project/'));
    }
 
    // delete user
    public function delete($id = null){
        $ProjectModel = new ProjectModel();
        $data['user'] = $ProjectModel->where('id', $id)->delete($id);
        $this->session->setFlashdata("success", "Project Deleted Sucessfully");
        return $this->response->redirect(site_url('/admin/project/'));
    }  
}
