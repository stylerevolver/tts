<?= $this->extend("layouts/app") ?>

<?= $this->section("body") ?>
<div class="toolbar py-2" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex align-items-center">
      	<div class="flex-grow-1 flex-shrink-0 me-5">
        	<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
          		<h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Dashboard
            		<span class="h-20px border-gray-200 border-start ms-3 mx-2"></span>
            		<small class="text-muted fs-7 fw-bold my-1 ms-1">User List</small>
          		</h1>
          		
          		
        	</div>

        </div>

    </div>
</div>

<div class="content d-flex flex-column flex-column-fluid" id="kt_content">

  	<div id="kt_content_container" class="container-xxl">
    	<div class="card">
    		<?php 
				if (session()->getFlashdata('success') && session()->getFlashdata('success') != ""){?>
					<div class="col-12" style="margin: 11px;width: 98%;">
						<div class="alert alert-success" role="alert">
						  <?php echo session()->getFlashdata('success');?>
						</div>
					</div>
				<?php }
				if (session()->getFlashdata('error') && session()->getFlashdata('error') != ""){?>
					<div class="col-12" style="margin: 11px;width: 98%;">
						<div class="alert alert-success" role="alert">
						  <?php echo session()->getFlashdata('error');?>
						</div>
					</div>
				<?php }
			 	if (isset($validation)) : ?>
			 	    <div class="col-12" style="margin: 11px;width: 98%;">
		                <div class="alert alert-danger" role="alert">
		                    <?= $validation->listErrors() ?>
		                </div>
		               </div>
		        
	        <?php endif; ?>
					
			<div class="card-body pt-0">
        		<div id="kt_customers_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
		          <div class="table-responsive">
		            <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer" id="Company_table" role="grid">
			            <thead>
			                <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0" role="row">
			                  <th class="min-w-125px sorting" style="width: 164.775px;">Name</th>
			                  <th class="min-w-125px sorting" style="width: 181.3px;">Email</th>
			                  <th class="min-w-125px sorting" style="width: 164.775px;">Phone NO</th>
			                  <th class="min-w-125px sorting" style="width: 164.775px;">Gender</th>
			                  <th class="min-w-125px sorting" style="width: 164.775px;">Address</th>
			                  <th class="min-w-125px sorting" style="width: 164.775px;">City Name</th>
			                  <th class="min-w-125px sorting" style="width: 164.775px;">Date Of Birth</th>
			                  <th class="min-w-125px sorting" style="width: 164.775px;">Profile Pic</th>
			                  <th class="min-w-125px sorting" style="width: 164.775px;">Role</th>
			                  <th class="min-w-125px sorting_disabled" rowspan="1" colspan="1" aria-label="Actions" style="width: 116.85px;">Actions</th>
			                </tr>
			            </thead>
		              	<tbody class="fw-bold text-gray-600">
		              		<?php
		              			foreach ($users as $key => $user) { 
		              				?>
		              			 	<tr class="odd">
					                  	<td><?php echo $user->name;?></td>
					                  	<td><?php echo $user->email;?></td>
					                  	<td><?php echo $user->phoneno;?></td>
					                  	<td><?php echo ucfirst($user->gender);?></td>
					                  	<td><?php echo $user->address;?></td>
					                  	<td><?php echo $user->city_name;?></td>
					                  	<td><?php echo $user->dob;?></td>
					                  	<td>
					                  		<img src="<?=base_url().'/profile_image/'.$user->profilepic?>" onerror="if (this.src != 'error.jpg') this.src = '<?php echo base_url('assets/media/avatars/150-26.jpg');?>';" alt="image"  height="150" width="150"/>
					                  	</td>
					                  	<td><?php echo ucfirst($user->role);?></td>
					                  	<td class="text-center">
											<a href="<?php echo base_url('admin/user_edit/'.$user->id);?>" class="menu-link px-3">Edit</a>
											<a href="<?php echo base_url('admin/user_delete/'.$user->id);?>" class="menu-link px-3"  onclick="return confirm('Are you sure do you want to delete this record?');">Delete</a>
										</td>
					                </tr>
		              		<?php } ?>
		                </tbody>
		            </table>
		          </div>
		        </div>
		    </div>
    	</div>
  	</div>
</div>

<?= $this->endSection() ?>