<?= $this->extend("layouts/app") ?>

<?= $this->section("body") ?>
<div class="toolbar py-2" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex align-items-center">
      	<div class="flex-grow-1 flex-shrink-0 me-5">
        	<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
          		<h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Dashboard
            		<span class="h-20px border-gray-200 border-start ms-3 mx-2"></span>
            		<small class="text-muted fs-7 fw-bold my-1 ms-1">Project Edit</small>
          		</h1>
        	</div>
        </div>
    </div>
</div>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div id="kt_content_container" class="container-xxl">
		<div class="card mb-5 mb-xl-10">
			<!--begin::Card header-->
			<div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
				<div class="card-title m-0">
					<h3 class="fw-bolder m-0">Project Edit</h3> 
				</div>
			</div>
			<?php 
				if (session()->getFlashdata('success') && session()->getFlashdata('success') != ""){?>
					<div class="col-12" style="margin: 11px;width: 98%;">
						<div class="alert alert-success" role="alert">
						  <?php echo session()->getFlashdata('success');?>
						</div>
					</div>
				<?php }
				if (session()->getFlashdata('error') && session()->getFlashdata('error') != ""){?>
					<div class="col-12" style="margin: 11px;width: 98%;">
						<div class="alert alert-success" role="alert">
						  <?php echo session()->getFlashdata('error');?>
						</div>
					</div>
				<?php }
			 	if (session()->getFlashdata('validation')) :  ?>
			 	    <div class="col-12" style="margin: 11px;width: 98%;">
		                <div class="alert alert-danger" role="alert">
		                    <?= session()->getFlashdata('validation')->listErrors() ?>
		                </div>
		               </div>
		    <?php endif; ?>
			
			<div id="kt_account_profile_details" class="collapse show">
				<!--begin::Form-->
				<form action="<?= base_url('admin/project_update') ?>" method="post" class="form fv-plugins-bootstrap5 fv-plugins-framework">
					<input type="hidden" name="id" class="form-control form-control-lg form-control-solid" placeholder="Company name" value="<?php echo $project['id'];?>">
					<div class="card-body border-top p-9">
						<div class="row mb-6">
							<label class="col-lg-4 col-form-label required fw-bold fs-6">Name</label>
							<div class="col-lg-8 fv-row fv-plugins-icon-container">
								<input type="text" name="name" class="form-control form-control-lg form-control-solid" placeholder="Company name" value="<?php echo $project['name'];?>">
								<div class="fv-plugins-message-container invalid-feedback"></div>
							</div>
						</div>
						<div class="row mb-6">
							<label class="col-lg-4 col-form-label fw-bold fs-6">Description</label>
							<div class="col-lg-8 fv-row">
								<textarea name="description" class="form-control form-control-lg form-control-solid" placeholder="Enter the project description"><?php echo $project['description'];?></textarea>
							</div>
						</div>
						<div class="row mb-6">
							<label class="col-lg-4 col-form-label fw-bold fs-6"> <span class="required">Company</span> <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="" data-bs-original-title="Company" aria-label="Company"></i> </label>
							<div class="col-lg-8 fv-row fv-plugins-icon-container">
								<select name="company" aria-label="Select a Company" data-control="select2" data-placeholder="Select a Company" class="form-select form-select-solid form-select-lg fw-bold select2-hidden-accessible" data-select2-id="select2-data-7-mby1" tabindex="-1" aria-hidden="true">
									<option value="" data-select2-id="select2-data-9-nj9e" selected="selected">Select a company...</option>
									<?php 
										foreach ($company as $key => $c) { ?>
											<option value="<?php echo $c['id'];?>"
												<?php 
													if ($c['id'] == $project['company_id']){ ?>
														selected="selected"
												<?php 	}
												?>
												><?php echo $c['name'];?></option>
										<?php }
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="card-footer d-flex justify-content-end py-6 px-9">
						<button type="reset" class="btn btn-light btn-active-light-primary me-2">Discard</button>
						<button type="submit" class="btn btn-primary" id="kt_account_profile_details_submit">Save Changes</button>
					</div>
					<input type="hidden">
					<div></div>
				</form>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection() ?>