<?= $this->extend("layouts/app") ?>

<?= $this->section("body") ?>
<div class="toolbar py-2" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex align-items-center">
      	<div class="flex-grow-1 flex-shrink-0 me-5">
        	<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
          		<h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Dashboard
            		<span class="h-20px border-gray-200 border-start ms-3 mx-2"></span>
            		<small class="text-muted fs-7 fw-bold my-1 ms-1">Ticket List</small>
          		</h1>
          		
          		
        	</div>

        </div>

    </div>
</div>

<div class="content d-flex flex-column flex-column-fluid" id="kt_content">

  	<div id="kt_content_container" class="container-xxl">
    	<div class="card">
    		<?php 
				if (session()->getFlashdata('success') && session()->getFlashdata('success') != ""){?>
					<div class="col-12" style="margin: 11px;width: 98%;">
						<div class="alert alert-success" role="alert">
						  <?php echo session()->getFlashdata('success');?>
						</div>
					</div>
				<?php }
				if (session()->getFlashdata('error') && session()->getFlashdata('error') != ""){?>
					<div class="col-12" style="margin: 11px;width: 98%;">
						<div class="alert alert-success" role="alert">
						  <?php echo session()->getFlashdata('error');?>
						</div>
					</div>
				<?php }
			 	if (isset($validation)) : ?>
			 	    <div class="col-12" style="margin: 11px;width: 98%;">
		                <div class="alert alert-danger" role="alert">
		                    <?= $validation->listErrors() ?>
		                </div>
		               </div>
		        
	        <?php endif; ?>
					
			<div class="card-body pt-0">
        		<div id="kt_customers_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
		          <div class="table-responsive">
		            <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer table-responsive" id="Company_table" role="grid">
			            <thead>
			                <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0" role="row">
			                  <th class="min-w-125px">Name</th>
			                  <th class="min-w-125px">Description</th>
			                  <th class="min-w-125px">User Name</th>
			                  <th class="min-w-125px">Project Name</th>
			                  <th class="min-w-125px">Comapny Name</th>
			                  <th class="min-w-125px">Start Date</th>
			                  <th class="min-w-125px">End Date</th>
			                  <th class="min-w-125px">Status</th>
			                  <th class="min-w-125px" rowspan="1" colspan="1" aria-label="Actions" style="width: 116.85px;">Actions</th>
			                </tr>
			            </thead>
		              	<tbody class="fw-bold text-gray-600">
		              		<?php
		              			foreach ($tickets as $key => $ticket) { 
		              				?>
		              			 	<tr class="odd">
					                  	<td><?php echo $ticket->name;?></td>
					                  	<td><?php echo $ticket->description;?></td>
					                  	<td><?php echo $ticket->user_name;?></td>
					                  	<td><?php echo $ticket->project_name;?></td>
					                  	<td><?php echo $ticket->company_name;?></td>
					                  	<td><?php echo $ticket->startdate;?></td>
					                  	<td><?php echo $ticket->enddate;?></td>
					                  	<td><?php echo $ticket->status;?></td>
					                  	<td class="text-center">
											<a href="<?php echo base_url('admin/time_tracker_edit/'.$ticket->id);?>" class="menu-link px-3">Edit</a>
											<a href="<?php echo base_url('admin/time_tracker_delete/'.$ticket->id);?>" class="menu-link px-3" data-kt-customer-table-filter="delete_row" onclick="return confirm('Are you sure do you want to delete this record?');">Delete</a>
											
										</td>
					                </tr>
		              		<?php } ?>
		                </tbody>
		            </table>
		          </div>
		        </div>
		    </div>
    	</div>
  	</div>
</div>

<?= $this->endSection() ?>