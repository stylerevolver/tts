<div id="kt_aside" class="aside pb-5 pt-5 pt-lg-0" data-kt-drawer="true" data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'80px', '300px': '100px'}" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_mobile_toggle">
    <div class="aside-logo py-8" id="kt_aside_logo">
        <a href="<?php echo base_url("/");?>" class="d-flex align-items-center"> <img alt="Logo" src="<?php echo base_url("assets/media/logos/logo-demo-6.svg");?>" class="h-45px logo" /> </a>
    </div>
    <div class="aside-menu flex-column-fluid" id="kt_aside_menu">
        <div class="hover-scroll-overlay-y my-2 my-lg-5 pe-lg-n1" id="kt_aside_menu_wrapper" data-kt-scroll="true" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer" data-kt-scroll-wrappers="#kt_aside, #kt_aside_menu" data-kt-scroll-offset="5px">
            <div class="menu menu-column menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500 fw-bold" id="#kt_aside_menu" data-kt-menu="true">
                <div class="menu-item py-2">
                    <a class="menu-link active menu-center" href="<?php echo base_url('/');?>" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                        <span class="menu-icon me-0">
                            <i class="bi bi-house fs-2"></i>
                        </span>
                        <span class="menu-title">Home</span> 
                    </a>
                </div>
                <?php 
                if(session()->get('admin')){ ?>
                    <div data-kt-menu-trigger="click" data-kt-menu-placement="right-start" class="menu-item py-2"> <span class="menu-link menu-center" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                        <span class="menu-icon me-0">
                          <i class="bi bi-file-earmark-lock fs-2"></i>
                        </span>
                        <span class="menu-title">Company</span> </span>
                        <div class="menu-sub menu-sub-dropdown w-225px px-1 py-4">
                            <div class="menu-item">
                                <div class="menu-content">
                                    <a class="" href="<?php echo base_url('/admin/company');?>" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                    <span class="menu-section fs-5 fw-bolder ps-1 py-1">View</span>
                                    </a>
                                </div>
                                <div class="menu-content"> 
                                    <a class="" href="<?php echo base_url('/admin/company_create');?>" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                        <span class="menu-section fs-5 fw-bolder ps-1 py-1">Create</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-kt-menu-trigger="click" data-kt-menu-placement="right-start" class="menu-item py-2"> <span class="menu-link menu-center" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                        <span class="menu-icon me-0">
                          <i class="bi bi-file-earmark-lock fs-2"></i>
                        </span>
                        <span class="menu-title">Project</span> </span>
                        <div class="menu-sub menu-sub-dropdown w-225px px-1 py-4">
                            <div class="menu-item">
                                <div class="menu-content">
                                    <a class="" href="<?php echo base_url('/admin/project');?>" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                    <span class="menu-section fs-5 fw-bolder ps-1 py-1">View</span>
                                    </a>
                                </div>
                                <div class="menu-content"> 
                                    <a class="" href="<?php echo base_url('/admin/project_create');?>" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                        <span class="menu-section fs-5 fw-bolder ps-1 py-1">Create</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-kt-menu-trigger="click" data-kt-menu-placement="right-start" class="menu-item py-2"> <span class="menu-link menu-center" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                        <span class="menu-icon me-0">
                          <i class="bi bi-file-earmark-lock fs-2"></i>
                        </span>
                        <span class="menu-title">Tickets</span> </span>
                        <div class="menu-sub menu-sub-dropdown w-225px px-1 py-4">
                            <div class="menu-item">
                                <div class="menu-content">
                                    <a class="" href="<?php echo base_url('/admin/time_tracker');?>" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                    <span class="menu-section fs-5 fw-bolder ps-1 py-1">View</span>
                                    </a>
                                </div>
                                <div class="menu-content"> 
                                    <a class="" href="<?php echo base_url('/admin/time_tracker_create');?>" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                        <span class="menu-section fs-5 fw-bolder ps-1 py-1">Create</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-kt-menu-trigger="click" data-kt-menu-placement="right-start" class="menu-item py-2"> <span class="menu-link menu-center" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                        <span class="menu-icon me-0">
                          <i class="bi bi-file-earmark-lock fs-2"></i>
                        </span>
                        <span class="menu-title">Users</span> </span>
                        <div class="menu-sub menu-sub-dropdown w-225px px-1 py-4">
                            <div class="menu-item">
                                <div class="menu-content">
                                    <a class="" href="<?php echo base_url('/admin/user');?>" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                    <span class="menu-section fs-5 fw-bolder ps-1 py-1">View</span>
                                    </a>
                                </div>
                                <div class="menu-content"> 
                                    <a class="" href="<?php echo base_url('/admin/user_create');?>" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                        <span class="menu-section fs-5 fw-bolder ps-1 py-1">Create</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>