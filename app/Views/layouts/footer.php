<script>
      var hostUrl = "assets/";
</script>
<!--begin::Javascript-->
<!--begin::Global Javascript Bundle(used by all pages)-->
<script src="<?php echo base_url("assets/plugins/global/plugins.bundle.js");?>"></script>
<script src="<?php echo base_url("assets/js/scripts.bundle.js");?>"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Page Vendors Javascript(used by this page)-->
<script src="<?php echo base_url("assets/plugins/custom/fullcalendar/fullcalendar.bundle.js");?>"></script>
<!--end::Page Vendors Javascript-->
<!--begin::Page Custom Javascript(used by this page)-->
<script src="<?php echo base_url("assets/js/custom/widgets.js");?>"></script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.js"></script>
<!--end::Page Custom Javascript-->
<!--end::Javascript-->
<script type="text/javascript">
      jQuery(document).ready( function () {
            jQuery('#Company_table').DataTable({
                  responsive: true
            });
      });
</script>