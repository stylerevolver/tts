<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddUserTimeTracker extends Migration
{
     public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'time_tracker_id' => [
                'type' => 'INT',
                'unsigned' => TRUE
            ],
            'time' => [
                'type' => 'time',
                'null' => true,
            ],
            'remarks' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => true
            ],
            'updated_at' => [
                'type' => 'datetime',
                'null' => true,
            ],
        'created_at datetime default current_timestamp',
        ]);
        $this->forge->addForeignKey('time_tracker_id', 'time_tracker', 'id','CASCADE','CASCADE');
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('user_time_tracker');
    }

    public function down()
    {
        $this->forge->dropTable('user_time_tracker');
    }
}
