<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use App\Models\StateModel;
use App\Models\CityModel;

class DefaultCityState extends Seeder
{
    public function run()
    {   
        $city_state = json_decode(file_get_contents("state_city.json"), true);
        $stateModel = new StateModel();
        $cityModel = new CityModel();
        foreach ($city_state as $key => $states) {
            $data = [
                'name' => $key,
            ];
            $stateModel->insert($data);
            if(count($states) >0){
                foreach ($states as $ckey => $value) {
                    $findState=$stateModel->where('name', $key)->first();
                    if ($value != ""){
                        $citydata = [
                            'name' => $value,
                            'state_id' => $findState['id'],
                        ];
                        $cityModel->insert($citydata);
                    }
                }
            }
            echo "Done";
        }
    }
}
