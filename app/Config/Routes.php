<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->match(['get', 'post'], 'login', 'UserController::login', ["filter" => "noauth"]);
$routes->match(['get', 'post'], 'myprofile', 'UserController::myprofile', ["filter" => "auth"]);
$routes->match(['get', 'post'], 'changepassword', 'UserController::changepassword', ["filter" => "auth"]);



// Admin routes
$routes->group("admin", ["filter" => "auth"], function ($routes) {
    $routes->get("/", "AdminController::index");

    //company
    $routes->get('company', 'CompanyController::index');
    $routes->get('company_create', 'CompanyController::create');
    $routes->post('company_store', 'CompanyController::store');
    $routes->get('company_edit/(:num)', 'CompanyController::singleCompany/$1');
    $routes->post('company_update', 'CompanyController::update');
    $routes->get('company_delete/(:num)', 'CompanyController::delete/$1');

    //project
    $routes->get('project', 'ProjectController::index');
    $routes->get('project_create', 'ProjectController::create');
    $routes->post('project_store', 'ProjectController::store');
    $routes->get('project_edit/(:num)', 'ProjectController::singleProject/$1');
    $routes->post('project_update', 'ProjectController::update');
    $routes->get('project_delete/(:num)', 'ProjectController::delete/$1');

    //project
    $routes->get('time_tracker', 'TimeTrakerController::index');
    $routes->get('time_tracker_create', 'TimeTrakerController::create');
    $routes->post('time_tracker_store', 'TimeTrakerController::store');
    $routes->get('time_tracker_edit/(:num)', 'TimeTrakerController::edit/$1');
    $routes->post('time_tracker_update', 'TimeTrakerController::update');
    $routes->get('time_tracker_delete/(:num)', 'TimeTrakerController::delete/$1');

    //user
    $routes->get('user', 'UserAdminController::index');
    $routes->get('user_create', 'UserAdminController::create');
    $routes->post('user_store', 'UserAdminController::store');
    $routes->get('user_edit/(:num)', 'UserAdminController::edit/$1');
    $routes->post('user_update', 'UserAdminController::update');
    $routes->get('user_delete/(:num)', 'UserAdminController::delete/$1');
});

// user routes
$routes->group("employee", ["filter" => "auth"], function ($routes) {
    $routes->get("/", "EmployeeController::index");
});

$routes->get('logout', 'UserController::logout');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
