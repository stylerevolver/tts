<?php

namespace App\Validation;

use App\Models\UserModel;

class Userrules
{
    public function validateUser(string $str, string $fields, array $data)
    {
        $model = new UserModel();
        $user = $model->where('email', $data['email'])
            ->first();

        if (!$user) {
            return false;
        }

        return password_verify($data['password'], $user['pass']);
    }

    public function validateEmail(string $str, string $fields, array $data){

        $db=\Config\Database::connect();
        $sql="SELECT COUNT(*) as count FROM `users` WHERE email='".$data['email']."' and id!='".$data['id']."'";
        $query = $db->query($sql);
        $user = $query->getResult();
        
        if ($user[0]->count < 1) {
            return true;
        }else{
            return false;
        }
    }
}
